# RemindMeBot
Reminder bot for Telegram.

**Note:** the goal of this project is to try out some things with .NET Core, Telegram.Bot and GitLab CI in combination with Docker. I'm not planning on actively maintaining this project, but Merge Requests are welcome!

## Setup
1. Install [.NET Core 2.0](https://www.microsoft.com/net/download/core)
1. In the `RemindMeBot` directory:
    1. Run `dotnet restore`
    1. Copy `Settings.Template.json` to `Settings.json`
    1. Adjust settings in `Settings.json`

## Docker
1. See `CI/update-remindmebot` for reference
