﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RemindMeBot.Data;
using RemindMeBot.Jobs;
using RemindMeBot.Services;
using RemindMeBot.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Telegram.Bot;

namespace RemindMeBot
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public sealed class Program
    {
        public static async Task<int> Main()
        {
            var serviceProvider = ServiceProviderFactory();
            var logger = serviceProvider.GetService<ILogger<Program>>();
            var settings = serviceProvider.GetService<Settings>();
            logger.LogInformation($"Version: {settings.Version}");

            logger.LogInformation("Updating database");
            using (var database = serviceProvider.GetService<DatabaseContext>())
                await database.Database.MigrateAsync();

            logger.LogInformation("Starting services");
            var serviceTasks = new List<Task<Exception>>
            {
                StartService<BotService>(serviceProvider, logger),
                StartService<ReminderService>(serviceProvider, logger)
            };

            logger.LogInformation("Services started");
            return await WaitForServices(serviceTasks);
        }

        private static IServiceProvider ServiceProviderFactory()
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddDbContext<DatabaseContext>(DatabaseContextFactory, ServiceLifetime.Transient, ServiceLifetime.Singleton)
                .AddSingleton(TelegramBotClientFactory)
                .AddSingleton<Settings>()
                .AddSingleton<BotService>()
                .AddSingleton<ReminderService>()
                .AddSingleton<ScheduleRemindersJob>()
                .AddSingleton<SendReminderJob>()
                .BuildServiceProvider();

            var settings = serviceProvider.GetService<Settings>();
            serviceProvider.GetService<ILoggerFactory>()
                .AddConsole(settings.DebugLogging ? LogLevel.Debug : LogLevel.Information);

            return serviceProvider;
        }

        private static void DatabaseContextFactory(IServiceProvider serviceProvider, DbContextOptionsBuilder options)
        {
            var settings = serviceProvider.GetService<Settings>();

            options.UseSqlite($"Data Source={settings.DatabasePath}");
        }

        private static ITelegramBotClient TelegramBotClientFactory(IServiceProvider serviceProvider)
        {
            var settings = serviceProvider.GetService<Settings>();

            return new TelegramBotClient(settings.TelegramBotToken);
        }

        private static Task<Exception> StartService<T>(IServiceProvider serviceProvider, ILogger logger) where T : IService
        {
            logger.LogInformation($"Starting {typeof(T).Name}");
            var service = serviceProvider.GetService<T>();

            return Task.Run(service.Start);
        }

        private static async Task<int> WaitForServices(ICollection<Task<Exception>> serviceTasks)
        {
            while (serviceTasks.Count > 0)
            {
                var serviceTask = await Task.WhenAny(serviceTasks);

                if (serviceTask.Result != null)
                    return 1;

                serviceTasks.Remove(serviceTask);
            }

            return 0;
        }
    }
}
