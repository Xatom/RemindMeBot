﻿using Quartz;
using Quartz.Spi;
using System;

namespace RemindMeBot
{
    internal sealed class DiJobFactory : IJobFactory
    {
        internal DiJobFactory(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        private IServiceProvider ServiceProvider { get; }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return ServiceProvider.GetService(bundle.JobDetail.JobType) as IJob;
        }

        public void ReturnJob(IJob job)
        {
        }
    }
}
