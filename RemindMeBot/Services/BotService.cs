﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NodaTime.TimeZones;
using RemindMeBot.Commands;
using RemindMeBot.Commands.Interfaces;
using RemindMeBot.Data;
using RemindMeBot.Data.Models;
using RemindMeBot.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace RemindMeBot.Services
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    internal sealed class BotService : Service<BotService>
    {
        public BotService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            var validTimeZones = new HashSet<string>(TzdbDateTimeZoneSource.Default.GetIds());

            ReminderService = serviceProvider.GetService<ReminderService>();
            Commands = new List<ICommand>
            {
                new DeleteReminderCommand(serviceProvider),
                new RemindAtCommand(serviceProvider, validTimeZones),
                new RemindInCommand(serviceProvider),
                new SetTimeZoneCommand(serviceProvider, validTimeZones),
                new VersionCommand(serviceProvider)
            };
        }

        private ReminderService ReminderService { get; }
        private List<ICommand> Commands { get; }

        [SuppressMessage("ReSharper", "FunctionNeverReturns")]
        protected override async Task Run()
        {
            var lastUpdateId = 0;

            while (true)
            {
                var updates = await Bot.GetUpdatesAsync(lastUpdateId + 1, timeout: 30,
                    allowedUpdates: new[] { UpdateType.MessageUpdate });

                if (updates.Length == 0)
                    continue;

                using (var database = DatabaseFactory())
                {
                    var scheduleMax = DateTime.UtcNow + Settings.ScheduleAhead;
                    var reminders = new List<Reminder>(updates.Length);

                    foreach (var update in updates)
                    {
                        lastUpdateId = update.Id;

                        Logger.LogDebug($"Update {update.Id}");
                        var reminder = await ProcessUpdate(update, database);

                        // Don't schedule reminders too far ahead, they are guaranteed to
                        // automatically be scheduled by the ScheduleRemindersJob after scheduleMax.
                        if (reminder != null && reminder.ReminderDateTime <= scheduleMax)
                            reminders.Add(reminder);
                    }

                    await database.SaveChangesAsync();
                    await ReminderService.ScheduleReminders(reminders);
                }
            }
        }

        private async Task<Reminder> ProcessUpdate(Update update, DatabaseContext database)
        {
            var message = update.Message;

            if (message.MigrateToChatId != 0)
                await MigrateChat(message, database);

            if (string.IsNullOrEmpty(message.Text))
                return default;

            foreach (var command in Commands)
            {
                (var isMatch, var reminderTime) = await command.Execute(message, database);

                if (!isMatch)
                    continue;

                if (reminderTime.HasValue)
                    return await SendReminderSet(message, reminderTime.Value, database);

                break;
            }

            return default;
        }

        private async Task MigrateChat(Message message, DatabaseContext database)
        {
            Logger.LogInformation($"Migrating chat {message.Chat.Id} to {message.MigrateToChatId}");

            var reminders = await database.Reminders.Where(r =>
                r.TelegramChatId == message.Chat.Id)
                .ToListAsync();

            foreach (var reminder in reminders)
            {
                // The messages before the migration don't exist anymore.
                reminder.TelegramChatId = message.MigrateToChatId;
                reminder.IsProcessed = true;
            }

            await ReminderService.UnscheduleReminders(reminders);

            await Bot.SendSafeTextMessageAsync(Logger, message.MigrateToChatId,
                "*All previous reminders have been deleted* because the " +
                "chat has been converted to a supergroup.");
        }

        private async Task<Reminder> SendReminderSet(Message message, DateTime reminderTime, DatabaseContext database)
        {
            if (reminderTime <= DateTime.UtcNow)
            {
                await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                    "The date has to be in the future.", message.MessageId);

                return default;
            }

            var botMessage = await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                $"I will remind you at *{reminderTime:yyyy-MM-dd HH:mm:ss}Z*.", message.MessageId);

            var reminder = new Reminder
            {
                ReminderDateTime = reminderTime,
                TelegramChatId = message.Chat.Id,
                TelegramMessageId = message.ReplyToMessage?.MessageId ?? message.MessageId,
                TelegramUserId = message.From.Id,
                BotTelegramMessageId = botMessage.MessageId
            };

            database.Reminders.Add(reminder);
            Logger.LogInformation($"Reminder added for chat {message.Chat.Id}");

            return reminder;
        }
    }
}
