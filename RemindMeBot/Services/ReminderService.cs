﻿using Quartz;
using Quartz.Impl;
using RemindMeBot.Data.Models;
using RemindMeBot.Jobs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace RemindMeBot.Services
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    internal sealed class ReminderService : Service<ReminderService>
    {
        public ReminderService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        private IServiceProvider ServiceProvider { get; }
        private IScheduler Scheduler { get; set; }

        internal async Task ScheduleReminders(IEnumerable<Reminder> reminders)
        {
            var job = JobBuilder.Create<SendReminderJob>()
                .Build();

            var triggers = new List<ITrigger>();
            foreach (var reminder in reminders)
            {
                Debug.Assert(reminder.ReminderId != 0, "Reminder must have been saved to database!");

                var triggerKey = GetTriggerKey(reminder);
                var trigger = TriggerBuilder.Create()
                    .StartAt(reminder.UtcReminderDateTime)
                    .UsingJobData(SendReminderJob.ReminderIdKey, reminder.ReminderId)
                    .WithIdentity(triggerKey)
                    .Build();

                triggers.Add(trigger);
            }

            await Scheduler.ScheduleJob(job, triggers, true);
        }

        internal async Task UnscheduleReminder(Reminder reminder)
        {
            var triggerKey = GetTriggerKey(reminder);

            await Scheduler.UnscheduleJob(triggerKey);
        }

        internal async Task UnscheduleReminders(IEnumerable<Reminder> reminders)
        {
            var triggerKeys = reminders.Select(GetTriggerKey)
                .ToList();

            await Scheduler.UnscheduleJobs(triggerKeys);
        }

        protected override async Task Run()
        {
            var schedulerFactory = new StdSchedulerFactory();

            Scheduler = await schedulerFactory.GetScheduler();
            Scheduler.JobFactory = new DiJobFactory(ServiceProvider);

            await Scheduler.Start();

            var job = JobBuilder.Create<ScheduleRemindersJob>()
                .Build();

            var trigger = TriggerBuilder.Create()
                .StartNow()
                .WithSimpleSchedule(b => b
                    .WithInterval(Settings.ScheduleAhead)
                    .RepeatForever())
                .Build();

            await Scheduler.ScheduleJob(job, trigger);
        }

        private static TriggerKey GetTriggerKey(Reminder reminder) =>
            new TriggerKey($"ReminderId-{reminder.ReminderId}");
    }
}
