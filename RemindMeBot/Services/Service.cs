﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RemindMeBot.Data;
using RemindMeBot.Services.Interfaces;
using System;
using System.Threading.Tasks;
using Telegram.Bot;

namespace RemindMeBot.Services
{
    internal abstract class Service<T> : IService
    {
        protected Service(IServiceProvider serviceProvider)
        {
            Bot = serviceProvider.GetService<ITelegramBotClient>();
            Settings = serviceProvider.GetService<Settings>();
            Logger = serviceProvider.GetService<ILogger<T>>();

            ServiceProvider = serviceProvider;
        }

        protected ITelegramBotClient Bot { get; }
        protected Settings Settings { get; }
        protected ILogger Logger { get; }

        private IServiceProvider ServiceProvider { get; }

        public async Task<Exception> Start()
        {
            try
            {
                await Run();
            }
            catch (Exception exception)
            {
                Logger.LogCritical(exception, "Service has stopped unexpectedly");
                return exception;
            }

            return default;
        }

        protected DatabaseContext DatabaseFactory() =>
            ServiceProvider.GetService<DatabaseContext>();

        protected abstract Task Run();
    }
}
