﻿using System;
using System.Threading.Tasks;

namespace RemindMeBot.Services.Interfaces
{
    internal interface IService
    {
        Task<Exception> Start();
    }
}
