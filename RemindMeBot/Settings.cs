﻿using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics.CodeAnalysis;

namespace RemindMeBot
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    internal sealed class Settings
    {
        public Settings()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile(SettingsPath)
                .Build();

            TelegramBotToken = configuration["TelegramBotToken"];
            TelegramBotName = configuration["TelegramBotName"];
            ScheduleAhead = TimeSpan.FromSeconds(Convert.ToInt32(configuration["ScheduleAheadSeconds"]));
            DebugLogging = Convert.ToBoolean(configuration["DebugLogging"]);
        }

        private string SettingsPath { get; } =
            Environment.GetEnvironmentVariable("RMB_SETTINGS_PATH") ?? "Settings.json";

        internal string DatabasePath { get; } =
            Environment.GetEnvironmentVariable("RMB_DATABASE_PATH") ?? "RemindMeBot.db";

        internal string Version { get; } =
            Environment.GetEnvironmentVariable("RMB_VERSION") ?? "Development";

        internal string TelegramBotToken { get; }
        internal string TelegramBotName { get; }
        internal TimeSpan ScheduleAhead { get; }
        internal bool DebugLogging { get; }
    }
}
