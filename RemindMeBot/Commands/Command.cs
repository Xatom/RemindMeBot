﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RemindMeBot.Commands.Interfaces;
using RemindMeBot.Data;
using RemindMeBot.Data.Models;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace RemindMeBot.Commands
{
    internal abstract class Command<T> : ICommand
    {
        protected Command(IServiceProvider serviceProvider)
        {
            var settings = serviceProvider.GetService<Settings>();

            Bot = serviceProvider.GetService<ITelegramBotClient>();
            EscapedBotName = Regex.Escape(settings.TelegramBotName);
            Logger = serviceProvider.GetService<ILogger<T>>();
        }

        protected ITelegramBotClient Bot { get; }
        protected string EscapedBotName { get; }
        protected ILogger Logger { get; }

        protected abstract Regex Regex { get; }

        public async Task<(bool isMatch, DateTime? reminderTime)> Execute(Message message, DatabaseContext database)
        {
            var match = Regex.Match(message.Text);

            if (!match.Success)
                return (false, default);

            if (await IsValidCommandUsage(match, message))
                return (true, await Execute(match, message, database));

            return (true, default);
        }

        protected async Task<UserInfo> GetUserInfo(int telegramUserId, DatabaseContext database)
        {
            Logger.LogDebug($"Retrieving UserInfo for user {telegramUserId}");

            var userInfo = await database.UserInfos.FirstOrDefaultAsync(u => u.TelegramUserId == telegramUserId) ??
                new UserInfo
                {
                    TelegramUserId = telegramUserId
                };

            if (userInfo.UserInfoId == 0)
                database.UserInfos.Add(userInfo);

            return userInfo;
        }

        protected abstract Task<bool> IsValidCommandUsage(Match match, Message message);

        protected abstract Task<DateTime?> Execute(Match match, Message message, DatabaseContext database);
    }
}
