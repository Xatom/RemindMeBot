﻿using RemindMeBot.Data;
using System;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RemindMeBot.Commands.Interfaces
{
    internal interface ICommand
    {
        Task<(bool isMatch, DateTime? reminderTime)> Execute(Message message, DatabaseContext database);
    }
}
