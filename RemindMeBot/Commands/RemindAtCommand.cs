﻿using NodaTime.Extensions;
using NodaTime.TimeZones;
using RemindMeBot.Data;
using RemindMeBot.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RemindMeBot.Commands
{
    internal sealed class RemindAtCommand : Command<RemindAtCommand>
    {
        internal RemindAtCommand(IServiceProvider serviceProvider, ISet<string> validTimeZones) : base(serviceProvider)
        {
            Regex = new Regex($@"^\/remindat(?:@{EscapedBotName})? *(?<Date>[0-9]{{4}}-[0-9]{{2}}-[0-9]{{2}})? *(?<Time>[0-9]{{2}}:[0-9]{{2}})?$",
                RegexOptions.Compiled);

            ValidTimeZones = validTimeZones;
        }

        protected override Regex Regex { get; }

        private ISet<string> ValidTimeZones { get; }

        protected override async Task<bool> IsValidCommandUsage(Match match, Message message)
        {
            if (match.Groups["Date"].Success || match.Groups["Time"].Success)
                return true;

            await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                "Usage: `/remindat yyyy-MM-dd HH:mm`.", message.MessageId);

            return false;
        }

        protected override async Task<DateTime?> Execute(Match match, Message message, DatabaseContext database)
        {
            var validDate = DateTime.TryParseExact(match.Groups["Date"].Value, "yyyy-MM-dd",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out var reminderDate);

            var validTime = TimeSpan.TryParseExact(match.Groups["Time"].Value, @"hh\:mm",
                CultureInfo.InvariantCulture, TimeSpanStyles.None, out var reminderTime);

            if (!validDate && match.Groups["Date"].Success || !validTime && match.Groups["Time"].Success)
            {
                await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                    "Please enter a valid date/time.", message.MessageId);

                return default;
            }

            if (reminderDate == default)
                reminderDate = DateTime.UtcNow.Date;

            var userInfo = await GetUserInfo(message.From.Id, database);
            return ApplyTimeZone(reminderDate + reminderTime, userInfo.TimeZoneId);
        }

        private DateTime ApplyTimeZone(DateTime dateTime, string timeZoneId)
        {
            if (!ValidTimeZones.Contains(timeZoneId))
                return dateTime;

            var timeZone = TzdbDateTimeZoneSource.Default.ForId(timeZoneId);
            var local = dateTime.ToLocalDateTime();

            return local.InZoneLeniently(timeZone).ToDateTimeUtc();
        }
    }
}
