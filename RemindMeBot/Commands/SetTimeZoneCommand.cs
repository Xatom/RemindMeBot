﻿using Microsoft.Extensions.Logging;
using RemindMeBot.Data;
using RemindMeBot.Helpers;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RemindMeBot.Commands
{
    internal sealed class SetTimeZoneCommand : Command<SetTimeZoneCommand>
    {
        internal SetTimeZoneCommand(IServiceProvider serviceProvider, ISet<string> validTimeZones) : base(serviceProvider)
        {
            Regex = new Regex($@"^\/settimezone(?:@{EscapedBotName})? *(?<TimeZone>.+)?$",
                RegexOptions.Compiled);

            ValidTimeZones = validTimeZones;
        }

        protected override Regex Regex { get; }

        private ISet<string> ValidTimeZones { get; }

        protected override async Task<bool> IsValidCommandUsage(Match match, Message message)
        {
            if (ValidTimeZones.Contains(match.Groups["TimeZone"].Value))
                return true;

            await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                "Usage: `/settimezone [timezone]`.", message.MessageId);

            return false;
        }

        protected override async Task<DateTime?> Execute(Match match, Message message, DatabaseContext database)
        {
            var timeZone = match.Groups["TimeZone"].Value;
            var userInfo = await GetUserInfo(message.From.Id, database);

            userInfo.TimeZoneId = timeZone;

            await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                $"Future input will automatically be converted from *{timeZone}*!", message.MessageId);

            Logger.LogInformation($"Time zone set for user {message.From.Id}");

            return default;
        }
    }
}
