﻿using RemindMeBot.Data;
using RemindMeBot.Helpers;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RemindMeBot.Commands
{
    internal sealed class RemindInCommand : Command<RemindInCommand>
    {
        internal RemindInCommand(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            Regex = new Regex($@"^\/remindin(?:@{EscapedBotName})? *(?<Amount>[0-9]+)?(?<Unit>min|h|d|w|m|y)?$",
                RegexOptions.Compiled);
        }

        protected override Regex Regex { get; }

        protected override async Task<bool> IsValidCommandUsage(Match match, Message message)
        {
            if (match.Groups["Amount"].Success && match.Groups["Unit"].Success)
                return true;

            await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                "Usage: `/remindin [0-9][min|h|d|w|m|y]`.", message.MessageId);

            return default;
        }

        protected override async Task<DateTime?> Execute(Match match, Message message, DatabaseContext database)
        {
            var amount = Convert.ToInt32(match.Groups["Amount"].Value);
            var unit = match.Groups["Unit"].Value;

            var reminderTime = GetReminderTime(amount, unit);

            if (reminderTime == default)
                await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                    "Please enter a valid unit.", message.MessageId);

            return reminderTime;
        }

        private static DateTime? GetReminderTime(int amount, string unit)
        {
            var reminderTime = DateTime.UtcNow;

            switch (unit)
            {
                case "min":
                    return reminderTime.AddMinutes(amount);

                case "h":
                    return reminderTime.AddHours(amount);

                case "d":
                    return reminderTime.AddDays(amount);

                case "w":
                    return reminderTime.AddDays(amount * 7);

                case "m":
                    return reminderTime.AddMonths(amount);

                case "y":
                    return reminderTime.AddYears(amount);

                default:
                    return default;
            }
        }
    }
}
