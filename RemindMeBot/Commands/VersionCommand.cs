﻿using Microsoft.Extensions.DependencyInjection;
using RemindMeBot.Data;
using RemindMeBot.Helpers;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RemindMeBot.Commands
{
    internal sealed class VersionCommand : Command<VersionCommand>
    {
        public VersionCommand(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            Regex = new Regex($@"^\/version(?:@{EscapedBotName})? *(?<Other>.+)?$",
                RegexOptions.Compiled);

            Settings = serviceProvider.GetService<Settings>();
        }

        protected override Regex Regex { get; }

        private Settings Settings { get; }

        protected override async Task<bool> IsValidCommandUsage(Match match, Message message)
        {
            if (!match.Groups["Other"].Success)
                return true;

            await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                "Usage: `/version`.", message.MessageId);

            return false;
        }

        protected override async Task<DateTime?> Execute(Match match, Message message, DatabaseContext database)
        {
            await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                $"Running *{Settings.Version}*", message.MessageId);

            return default;
        }
    }
}
