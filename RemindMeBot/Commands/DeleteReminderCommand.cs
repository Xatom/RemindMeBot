﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RemindMeBot.Data;
using RemindMeBot.Data.Models;
using RemindMeBot.Helpers;
using RemindMeBot.Services;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RemindMeBot.Commands
{
    internal sealed class DeleteReminderCommand : Command<DeleteReminderCommand>
    {
        internal DeleteReminderCommand(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            Regex = new Regex($@"^\/deletereminder(?:@{EscapedBotName})? *(?<Other>.+)?$",
                RegexOptions.Compiled);

            ReminderService = serviceProvider.GetService<ReminderService>();
        }

        protected override Regex Regex { get; }

        private ReminderService ReminderService { get; }

        protected override async Task<bool> IsValidCommandUsage(Match match, Message message)
        {
            if (!match.Groups["Other"].Success)
                return true;

            await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                "Usage: `/deletereminder`.", message.MessageId);

            return false;
        }

        protected override async Task<DateTime?> Execute(Match match, Message message, DatabaseContext database)
        {
            var messageId = message.ReplyToMessage?.MessageId;
            if (!messageId.HasValue)
                return default;

            var reminder = await database.Reminders.FirstOrDefaultAsync(r =>
                r.BotTelegramMessageId == messageId &&
                r.TelegramChatId == message.Chat.Id);

            if (!await IsDeletable(message, reminder))
                return default;

            reminder.IsProcessed = true;
            await ReminderService.UnscheduleReminder(reminder);

            await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                "The reminder has been deleted.", message.MessageId);

            return default;
        }

        private async Task<bool> IsDeletable(Message message, Reminder reminder)
        {
            if (reminder == null)
                return false;

            if (!reminder.IsProcessed)
            {
                if (message.From.Id == reminder.TelegramUserId)
                    return true;

                await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                    "You are not authorized to delete this reminder. " +
                    "Only the creator of the reminder can delete it.", message.MessageId);

                return false;
            }

            await Bot.SendSafeTextMessageAsync(Logger, message.Chat.Id,
                "The reminder has already been deleted.", message.MessageId);

            return false;
        }
    }
}
