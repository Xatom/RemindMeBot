﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RemindMeBot.Migrations
{
    public partial class AddTelegramChatMessage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "TelegramChatId",
                table: "Reminders",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "TelegramMessageId",
                table: "Reminders",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TelegramChatId",
                table: "Reminders");

            migrationBuilder.DropColumn(
                name: "TelegramMessageId",
                table: "Reminders");
        }
    }
}
