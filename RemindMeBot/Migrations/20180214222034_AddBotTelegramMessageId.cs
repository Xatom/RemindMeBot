﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RemindMeBot.Migrations
{
    public partial class AddBotTelegramMessageId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BotTelegramMessageId",
                table: "Reminders",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reminders_TelegramChatId_BotTelegramMessageId",
                table: "Reminders",
                columns: new[] { "TelegramChatId", "BotTelegramMessageId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Reminders_TelegramChatId_BotTelegramMessageId",
                table: "Reminders");

            migrationBuilder.DropColumn(
                name: "BotTelegramMessageId",
                table: "Reminders");
        }
    }
}
