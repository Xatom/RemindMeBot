﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RemindMeBot.Migrations
{
    public partial class AddTelegramUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TelegramUserId",
                table: "Reminders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TelegramUserId",
                table: "Reminders");
        }
    }
}
