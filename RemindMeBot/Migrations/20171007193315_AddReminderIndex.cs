﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RemindMeBot.Migrations
{
    public partial class AddReminderIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Reminders_IsProcessed_ReminderDateTime",
                table: "Reminders",
                columns: new[] { "IsProcessed", "ReminderDateTime" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Reminders_IsProcessed_ReminderDateTime",
                table: "Reminders");
        }
    }
}
