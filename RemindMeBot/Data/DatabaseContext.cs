﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using RemindMeBot.Data.Models;
using System.Diagnostics.CodeAnalysis;

namespace RemindMeBot.Data
{
    internal sealed class DatabaseContext : DbContext, IDesignTimeDbContextFactory<DatabaseContext>
    {
        [SuppressMessage("ReSharper", "UnusedMember.Global")]
        public DatabaseContext()
        {
        }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        public DatabaseContext(DbContextOptions options) : base(options)
        {
        }

        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public DbSet<Reminder> Reminders { get; set; }

        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public DbSet<UserInfo> UserInfos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Reminder>()
                .HasIndex(r => new { r.IsProcessed, r.ReminderDateTime });
            modelBuilder.Entity<Reminder>()
                .HasIndex(r => new { r.TelegramChatId, r.BotTelegramMessageId });

            modelBuilder.Entity<UserInfo>()
                .HasIndex(u => new { u.TelegramUserId });
        }

        public DatabaseContext CreateDbContext(string[] args)
        {
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<DatabaseContext>();
            dbContextOptionsBuilder.UseSqlite("Data Source=DesignTime.db");

            return new DatabaseContext(dbContextOptionsBuilder.Options);
        }
    }
}
