﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace RemindMeBot.Data.Models
{
    internal sealed class Reminder
    {
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public int ReminderId { get; set; }

        /// <summary>
        /// DateTime to send the reminder at.
        /// Should always be in UTC!
        /// </summary>
        public DateTime ReminderDateTime { get; set; }

        public int? TelegramUserId { get; set; }

        public long TelegramChatId { get; set; }

        public int TelegramMessageId { get; set; }

        public int? BotTelegramMessageId { get; set; }

        public bool IsProcessed { get; set; }

        /// <summary>
        /// Helper to retrieve the reminder DateTime in UTC.
        /// When retrieving from the database this is Unspecified.
        /// </summary>
        public DateTime UtcReminderDateTime =>
            DateTime.SpecifyKind(ReminderDateTime, DateTimeKind.Utc);
    }
}
