﻿using System.Diagnostics.CodeAnalysis;

namespace RemindMeBot.Data.Models
{
    internal sealed class UserInfo
    {
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public int UserInfoId { get; set; }

        public int TelegramUserId { get; set; }

        public string TimeZoneId { get; set; }
    }
}
