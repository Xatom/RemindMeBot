﻿using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace RemindMeBot.Helpers
{
    internal static class BotExtensions
    {
        internal static async Task<Message> SendSafeTextMessageAsync(this ITelegramBotClient bot, ILogger logger, ChatId chatId, string text, int replyToMessageId = 0)
        {
            try
            {
                return await bot.SendTextMessageAsync(chatId, text, ParseMode.Markdown, replyToMessageId: replyToMessageId);
            }
            catch (ApiRequestException exception)
            {
                logger.LogError(exception, replyToMessageId == default ?
                    $"Failed to send message to chat {chatId}" :
                    $"Failed to send message to chat {chatId} in reply to {replyToMessageId}");

                return default;
            }
        }
    }
}
