﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using RemindMeBot.Helpers;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Telegram.Bot;

namespace RemindMeBot.Jobs
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    internal sealed class SendReminderJob : Job<SendReminderJob>
    {
        public SendReminderJob(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            Bot = serviceProvider.GetService<ITelegramBotClient>();
        }

        internal const string ReminderIdKey = "ReminderId";

        private ITelegramBotClient Bot { get; }

        public override async Task Execute(IJobExecutionContext context)
        {
            Logger.LogInformation("Sending reminder");

            using (var database = DatabaseFactory())
            {
                var reminderId = context.MergedJobDataMap.GetIntValue(ReminderIdKey);
                var reminder = await database.Reminders.FindAsync(reminderId);

                if (reminder.IsProcessed)
                    return;

                await Bot.SendSafeTextMessageAsync(Logger, reminder.TelegramChatId,
                    "Here's your reminder!", reminder.TelegramMessageId);

                reminder.IsProcessed = true;

                await database.SaveChangesAsync();
            }
        }
    }
}
