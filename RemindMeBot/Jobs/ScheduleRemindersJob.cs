﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using RemindMeBot.Services;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace RemindMeBot.Jobs
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    internal sealed class ScheduleRemindersJob : Job<ScheduleRemindersJob>
    {
        public ScheduleRemindersJob(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            ReminderService = serviceProvider.GetService<ReminderService>();
            Settings = serviceProvider.GetService<Settings>();
        }

        private ReminderService ReminderService { get; }
        private Settings Settings { get; }

        public override async Task Execute(IJobExecutionContext context)
        {
            Logger.LogInformation("Scheduling reminders");

            using (var database = DatabaseFactory())
            {
                var scheduleMax = DateTime.UtcNow + Settings.ScheduleAhead;
                var reminders = database.Reminders.Where(r =>
                        !r.IsProcessed &&
                        r.ReminderDateTime <= scheduleMax)
                    .ToList();

                if (reminders.Count == 0)
                    return;

                await ReminderService.ScheduleReminders(reminders);
            }
        }
    }
}
