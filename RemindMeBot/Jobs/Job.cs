﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using RemindMeBot.Data;
using System;
using System.Threading.Tasks;

namespace RemindMeBot.Jobs
{
    internal abstract class Job<T> : IJob
    {
        protected Job(IServiceProvider serviceProvider)
        {
            Logger = serviceProvider.GetService<ILogger<T>>();

            ServiceProvider = serviceProvider;
        }

        protected ILogger Logger { get; }

        private IServiceProvider ServiceProvider { get; }

        public abstract Task Execute(IJobExecutionContext context);

        protected DatabaseContext DatabaseFactory() =>
            ServiceProvider.GetService<DatabaseContext>();
    }
}
