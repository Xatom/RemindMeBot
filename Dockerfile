FROM microsoft/dotnet:latest AS build

COPY . /build
WORKDIR /build

RUN dotnet publish -c Release -o out

FROM microsoft/dotnet:runtime

COPY --from=build /build/RemindMeBot/out /app
WORKDIR /app

COPY RemindMeBot/Settings.Template.json /data/Settings.json
VOLUME /data

ARG version=Test

ENV RMB_SETTINGS_PATH /data/Settings.json
ENV RMB_DATABASE_PATH /data/RemindMeBot.db
ENV RMB_VERSION=${version}

ENTRYPOINT ["dotnet", "RemindMeBot.dll"]
