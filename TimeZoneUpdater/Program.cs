﻿using NodaTime.TimeZones;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;

namespace TimeZoneUpdater
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class Program
    {
        public static void Main()
        {
            var timeZoneList = new TimeZoneList();
            var zoneLocations = TzdbDateTimeZoneSource.Default.ZoneLocations;

            if (zoneLocations == null)
                throw new Exception("No zone locations available");

            foreach (var zoneLocation in zoneLocations.OrderBy(zl => zl.ZoneId))
                timeZoneList.AddTimeZone(new TimeZone
                {
                    ZoneId = zoneLocation.ZoneId,
                    Countries = string.Join(", ", $"{zoneLocation.CountryName} ({zoneLocation.CountryCode})"),
                    Comment = zoneLocation.Comment
                });

            Console.WriteLine("Enter target filename:");
            Console.Write("> ");

            var targetFilename = Console.ReadLine();
            var markdown = timeZoneList.ToMarkdown();
            File.WriteAllText(targetFilename, markdown);
        }

        private class TimeZoneList
        {
            private readonly List<TimeZone> _timeZones = new List<TimeZone>();

            private int _maxZoneIdLength;
            private int _maxCountriesLength;
            private int _maxCommentLength;

            internal void AddTimeZone(TimeZone timeZone)
            {
                _maxZoneIdLength = Math.Max(_maxZoneIdLength, timeZone.ZoneId.Length);
                _maxCountriesLength = Math.Max(_maxCountriesLength, timeZone.Countries.Length);
                _maxCommentLength = Math.Max(_maxCommentLength, timeZone.Comment.Length);

                _timeZones.Add(timeZone);
            }

            internal string ToMarkdown()
            {
                var builder = new StringBuilder();

                builder.AppendLine("Valid time zones to be used with this bot.");
                builder.AppendLine("Use the zone ID to set the time zone, for example: `/settimezone Europe/Amsterdam`.");

                builder.AppendLine($"| {"Zone ID".PadRight(_maxZoneIdLength)} | {"Countries".PadRight(_maxCountriesLength)} | {"Comment".PadRight(_maxCommentLength)} |");
                builder.AppendLine($"|{new string('-', _maxZoneIdLength + 2)}|{new string('-', _maxCountriesLength + 2)}|{new string('-', _maxCommentLength + 2)}|");

                foreach (var timeZone in _timeZones)
                    builder.AppendLine($"| {timeZone.ZoneId.PadRight(_maxZoneIdLength)} | {timeZone.Countries.PadRight(_maxCountriesLength)} | {timeZone.Comment.PadRight(_maxCommentLength)} |");

                return builder.ToString();
            }
        }

        private class TimeZone
        {
            internal string ZoneId { get; set; }

            internal string Countries { get; set; }

            internal string Comment { get; set; }
        }
    }
}
